package main

import (
	"bufio"
	"context"
	"flag"
	"log"
	"os"
	"redis-lookup/pkg/redis"
	"strings"
	"sync"

	_ "github.com/joho/godotenv/autoload"
)

const KeysTxt = "files/keys.txt"

var UserId = flag.String("user", "2679898", "The ID of the user")

func main() {
	flag.Parse()
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")
	password := os.Getenv("REDIS_PASSWORD")
	useSSL := strings.ToLower(os.Getenv("REDIS_SSL")) == "true"

	redis := redis.NewClient(host, port, password, useSSL)

	redis.Ping(context.Background())

	log.Println("Fetching all keys")
	writeKeys(*redis)
	log.Println("Finding hit!")
	findInKeys(*redis)
}

func findHit(section []string, redisClient redis.RedisClient, channel chan redis.RedisHit, wg *sync.WaitGroup, ctx context.Context) {

	len := len(section)
	for index, line := range section {
		session := redisClient.Find(line, "UserID", *UserId, ctx)
		log.Printf("Thread is %.1f%% done! (%d, %d)\n", (float64(index)/float64(len))*100., index, len)
		if session != nil {
			// log.Println("Got a hit!", session)
			hit := redis.RedisHit{
				RedisKey: line,
				Session:  *session,
			}
			channel <- hit
			wg.Done()
		}
	}

	wg.Done()
}

func findInKeys(redisClient redis.RedisClient) {
	bytes, err := os.ReadFile(KeysTxt)
	if err != nil {
		log.Fatal("Error opening keys file:", err)
	}
	lines := strings.Split(string(bytes), "\n")

	for i, j := 0, len(lines)-1; i < j; i, j = i+1, j-1 {
		lines[i], lines[j] = lines[j], lines[i]
	}

	threads := 10
	stepSize := len(lines) / threads

	ctx := context.Background()

	var wg sync.WaitGroup
	channel := make(chan redis.RedisHit, 1)

	for i := 0; i < 1; i++ {
		min := i * stepSize
		max := min + stepSize
		wg.Add(1)
		go findHit(lines[min:max], redisClient, channel, &wg, ctx)
	}

	wg.Wait()
	close(channel)

	if len(channel) == 0 {
		log.Println("Found nothing")
		return
	}

	for hit := range channel {
		log.Println("Session key:", hit.RedisKey)
		log.Println("\tUserID:", hit.Session.UserID)
		log.Println("\tTokenID:", hit.Session.TokenID)
		log.Println("\tSchoolID:", hit.Session.SchoolID)
	}
}

func writeKeys(redis redis.RedisClient) {
	ctx := context.Background()
	keys := redis.FetchKeys("s:*", ctx)

	files, err := os.Create(KeysTxt)
	if err != nil {
		log.Fatal("Error opening file")
	}
	defer files.Close()

	writer := bufio.NewWriter(files)

	for _, key := range keys {
		writer.WriteString(key)
		writer.WriteString("\n")
	}
	writer.Flush()
}
