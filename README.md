# Redis Lookup

How to run:
```bash
ssh bastion -nNTf -4 -L 6379:master.ready-prod-api-cache.akgezz.use1.cache.amazonaws.com:6379
export REDIS_PASSWORD="***"
echo "KEYS *" | redis-cli --tls -a $REDIS_PASSWORD -p 6379
```

Note when filling out the environment, don't specify localhost but 127.0.0.1