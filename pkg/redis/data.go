package redis

type UserSession struct {
	UserID               string `redis:"user_id"`
	Ver                  string `redis:"ver"`
	TokenID              string `redis:"token_id"`
	ExtraInfo            string `redis:"extra_info"`
	UpdateAvailTimeEpoch string `redis:"update_avail_time_epoch"`
	ExpireTimeEpoch      string `redis:"expire_time_epoch"`
	DeviceType           string `redis:"device_type"`
	UpdateReqTimeEpoch   string `redis:"update_req_time_epoch"`
	UserAgent            string `redis:"user_agent"`
	SchoolID             string `redis:"school_id"`
}

type RedisHit struct {
	RedisKey string
	Session  UserSession
}
