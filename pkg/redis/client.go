package redis

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"reflect"

	"github.com/redis/go-redis/v9"
)

type RedisClient struct {
	client *redis.Client
}

func NewClient(host, port, password string, useSsl bool) *RedisClient {
	client := RedisClient{}

	var tlsConfig *tls.Config

	if useSsl {
		tlsConfig = &tls.Config{
			InsecureSkipVerify: true,
		}
	}

	redisClient := redis.NewClient(&redis.Options{
		Addr:      fmt.Sprintf("%s:%s", host, port),
		Password:  password,
		TLSConfig: tlsConfig,
	})

	client.client = redisClient

	return &client
}

func (r *RedisClient) Ping(ctx context.Context) {
	pong, err := r.client.Ping(ctx).Result()
	if err != nil {
		log.Fatal("[PING] Error: ", err)
	} else {
		log.Println("[PING] Pong:", pong)
	}
}

func (r *RedisClient) FetchKeys(id string, ctx context.Context) []string {
	keys := []string{}

	iter := r.client.Scan(ctx, 0, id, 0).Iterator()
	for iter.Next(ctx) {
		key := iter.Val()
		keys = append(keys, key)
		log.Println("Key length:", len(keys))
	}
	if err := iter.Err(); err != nil {
		log.Fatalln("Error finding iteration:", err)
	}

	return keys
}

func (r *RedisClient) Find(id, key, value string, ctx context.Context) *UserSession {
	var model UserSession
	if err := r.client.HGetAll(ctx, id).Scan(&model); err != nil {
		log.Fatal("Error fetching object:", err)
	}

	val := r.getValue(model, key)
	// log.Println("Checking:", val, "==", value)
	if val == value {
		return &model
	}

	return nil
}

func (r *RedisClient) getValue(user UserSession, field string) string {
	val := reflect.ValueOf(user)
	reflectField := reflect.Indirect(val).FieldByName(field)

	return reflectField.String()
}
